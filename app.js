// Config environment
const express = require("express"); // Import express
const fs = require("fs");
const path = require("path");
const mongoSanitize = require("express-mongo-sanitize");
const xss = require("xss-clean");
const rateLimit = require("express-rate-limit");
const hpp = require("hpp");
const helmet = require("helmet");
const cors = require("cors");
const morgan = require("morgan");

const app = express(); // Make express app

// CORS
app.use(cors());

// Sanitize data
app.use(mongoSanitize());

// Prevent XSS attact
app.use(xss());

// Rate limiting
const limiter = rateLimit({
  windowMs: 1 * 60 * 1000, // 1 mins
  max: 100,
});

app.use(limiter);

// Prevent http param pollution
app.use(hpp());

// Use helmet
app.use(
  helmet({
    contentSecurityPolicy: false,
  })
);

/* Enables req.body */
app.use(express.json()); // Enables req.body (JSON)
// Enables req.body (url-encoded)
app.use(
  express.urlencoded({
    extended: true,
  })
);

function sortStr(str) {
  const newStr = str.split("");
  for (let i = 0; i < newStr.length; i++) {
    for (let j = i; j < newStr.length; j++) {
      if (newStr[i] > newStr[j]) {
        let temp = newStr[i];
        newStr[i] = newStr[j];
        newStr[j] = temp;
      }
    }
  }
  return newStr.join("");
}

app.post("/", (req, res) => {
  let name = req.body.name;

  res.json({ message: sortStr(name) });
});

/* Run the server */
if (process.env.NODE_ENV || process.env.PORT !== "test") {
  app.listen(3000, () => console.log(`Server running on 3000`));
}

module.exports = app;
